const CouchbaseLib = require('./../libs/couchbase');
const faker = require('faker');

const couchbaseClient = new CouchbaseLib();

const createConversations = async () => {
  const docs = [];
  for (let index = 0; index < 50; index++) {
    docs.push({
      id: faker.random.uuid(),
      customer_id: faker.random.number(12),
      rate: faker.random.number({min:1, max:6}),
      created_at: faker.date.between('2020-01-01', '2020-05-31'),
      type: 'conversation'
    });
  }
  return await couchbaseClient.insertDocuments(docs);
}

const run = async () => {
  await createConversations();
  await createConversations();
  await createConversations();
  await createConversations();
  await createConversations();
  await createConversations();
  await createConversations();
  await createConversations();
  await createConversations();
  await createConversations();
  await createConversations();
  console.log('end');
}

run();