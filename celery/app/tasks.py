import requests
from celery import Celery
from celery.schedules import crontab
import logging

celery_app = Celery('tasks', broker='redis://redis:6379/')
celery_app.conf.timezone = 'America/Bogota'

API = 'https://api.nicobytes.site/api/conversations/mocks'

logger = logging.getLogger(__name__)

celery_app.conf.beat_schedule = {
    'each 5 min': {
        'task': 'tasks.run_scraper',
        'schedule': crontab(minute='*/5')
    },
}


@celery_app.task()
def run_scraper():
    return requests.get(API).json()
